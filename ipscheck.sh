#!/bin/bash
#Script to check for changes on Cloudflare/Sucuri IPs
#Lists are:
# https://www.cloudflare.com/ips-v4
# https://www.cloudflare.com/ips-v6
#  curl -iSs https://docs.sucuri.net/website-firewall/troubleshooting/same-ip-for-all-users | grep -oP '(?<=set_real_ip_from )[0-9a-f\.:/]+(?=;)'

# Sucuri
if [[ ! -f /tmp/sucurilist.txt ]]; then
    curl -iSs https://docs.sucuri.net/website-firewall/troubleshooting/same-ip-for-all-users | grep -oP '(?<=set_real_ip_from )[0-9a-f\.:/]+(?=;)' >> /tmp/sucurilist.txt
    # For when the required file does not exist, we populate it on the first run, the following day the next block should execute.
    if [[ ! -s /tmp/sucurilist.txt ]]; then
        logger $0 Sucuri returned an empty IP list!
        rm -f /tmp/sucurilist.txt
        exit 1
    fi # This if checks if the file we just got is empty, if it is we're bailing out as the diff check at the end will always trigger an email.
else
    if [[ -f /tmp/sucuritmp.txt ]]; then
        rm -f /tmp/sucurilist.txt
        mv /tmp/sucuritmp.txt /tmp/sucurilist.txt
    fi
    curl -iSs https://docs.sucuri.net/website-firewall/troubleshooting/same-ip-for-all-users | grep -oP '(?<=set_real_ip_from )[0-9a-f\.:/]+(?=;)' >> /tmp/sucuritmp.txt
    output=$(diff /tmp/sucuritmp.txt /tmp/sucurilist.txt)
    if [[ ! $? -eq 0 ]]; then
        echo $output | mail -s "Change in Sucuri IPs" reece.palmer@nublue.co.uk
    fi
fi

# Cloudflare4
if [[ ! -f /tmp/cloudflare4list.txt ]]; then
    curl -s https://www.cloudflare.com/ips-v4 >> /tmp/cloudflare4list.txt
    # For when the required file does not exist, we populate it on the first run, the following day the next block should execute.
else
    if [[ -f /tmp/cloudflare4tmp.txt ]]; then
        rm -f /tmp/cloudflare4list.txt
        mv /tmp/cloudflare4tmp.txt /tmp/cloudflare4list.txt
    fi
    curl -s https://www.cloudflare.com/ips-v4 >> /tmp/cloudflare4tmp.txt
    output=$(diff /tmp/cloudflare4tmp.txt /tmp/cloudflare4list.txt)
    if [[ ! $? -eq 0 ]]; then
        echo $output | mail -s "Change in Cloudflare IPv4 IPs" reece.palmer@nublue.co.uk
    fi
fi

# Cloudflare6
if [[ ! -f /tmp/cloudflare6list.txt ]]; then
    curl -s https://www.cloudflare.com/ips-v6 >> /tmp/cloudflare6list.txt
    # For when the required file does not exist, we populate it on the first run, the following day the next block should execute.
else
    if [[ -f /tmp/cloudflare6tmp.txt ]]; then
        rm -f /tmp/cloudflare6list.txt
        mv /tmp/cloudflare6tmp.txt /tmp/cloudflare6list.txt
    fi
    curl -s https://www.cloudflare.com/ips-v6 >> /tmp/cloudflare6tmp.txt
    output=$(diff /tmp/cloudflare6tmp.txt /tmp/cloudflare6list.txt)
    if [[ ! $? -eq 0 ]]; then
        echo $output | mail -s "Change in Cloudflare IPv6 IPs" reece.palmer@nublue.co.uk
    fi
fi