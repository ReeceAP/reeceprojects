#!/bin/bash
#Script to check for changes on Cloudflare/Sucuri IPs
#Lists are:
# https://www.cloudflare.com/ips-v4
# https://www.cloudflare.com/ips-v6
#  curl -iSs https://docs.sucuri.net/website-firewall/troubleshooting/same-ip-for-all-users | grep -oP '(?<=set_real_ip_from )[0-9a-f\.:/]+(?=;)'


if [[ ! -f /tmp/sucuristatic.txt ]]; then
    logger "Sucuri static file missing, please populate /tmp/sucuristatic.txt"
else
    curl -iSs https://docs.sucuri.net/website-firewall/troubleshooting/same-ip-for-all-users | grep -oP '(?<=set_real_ip_from )[0-9a-f\.:/]+(?=;)' >> /tmp/sucurifresh.txt
    if [[ ! -s /tmp/sucurifresh.txt ]]; then
        logger "Sucuri fresh file was empty!"
        exit 1
    else
        output=$(diff /tmp/sucurifresh.txt /tmp/sucuristatic.txt)
        if [[ ! $? -eq 0 ]]; then
            logger "Found change in sucuri IPs."
            echo $output | mail -s "Change in Sucuri IPs" reece.palmer@nublue.co.uk
        fi
    fi
fi

if [[ ! -f /tmp/cf4static.txt ]]; then
    logger "cf4 static file missing, please populate /tmp/cf4static.txt"
else
    curl -s https://www.cloudflare.com/ips-v4 >> /tmp/c4fresh.txt 
    if [[ ! -s /tmp/cf4fresh.txt ]]; then
        logger "cf4 fresh file was empty!"
    else
        output=$(diff /tmp/cf4fresh.txt /tmp/cf4static.txt)
        if [[ ! $? -eq 0 ]]; then
            logger "Found change in cloudflarev4 IPs."
            echo $output | mail -s "Change in cloudflarev4 IPs" reece.palmer@nublue.co.uk
        fi
    fi
fi

if [[ ! -f /tmp/cf6static.txt ]]; then
    logger "cf4 static file missing, please populate /tmp/cf6static.txt"
else
    curl -s https://www.cloudflare.com/ips-v6 >> /tmp/c6fresh.txt 
    if [[ ! -s /tmp/cf6fresh.txt ]]; then
        logger "cf6 fresh file was empty!"
    else
        output=$(diff /tmp/cf6fresh.txt /tmp/cf6static.txt)
        if [[ ! $? -eq 0 ]]; then
            logger "Found change in cloudflarev6 IPs."
            echo $output | mail -s "Change in cloudflarev6 IPs" reece.palmer@nublue.co.uk
        fi
    fi
fi